#!/bin/sh

sudo rm -rf debian-installer*

if [ "$1" = "bullseye" ] ;  then 
	dget -d https://deb.debian.org/debian/pool/main/d/debian-installer/debian-installer_20210731+deb11u8.dsc
elif [ "$1" = "bookworm" ] ;  then
	dget -d https://deb.debian.org/debian/pool/main/d/debian-installer/debian-installer_20230607+deb12u1.dsc
else
	echo "error"
	exit 1
fi

dpkg-source -x *.dsc
mv debian-installer-* debian-installer

cp /home/iwamatsu/dev/debian/maint/kernel/build-linux-kernel-package/*.udeb debian-installer/build/localudebs/.

docker run --rm -it --init \
		-e USER_ID=$(id -u) -e GROUP_ID=$(id -g) \
		-v $(pwd):/workdir --net="host" \
		docker.io/niwamatsu/cip-kernel-package-build-bullseye:latest \
		bash -c "./build.sh"
