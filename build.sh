#!/bin/sh

apt update
apt build-dep -y debian-installer
apt install -y linux-base initramfs-tools

cd debian-installer/build

LINUX_KERNEL_ABI=5.10.0-23 ./daily-build build
